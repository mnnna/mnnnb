# template-go-sdk

This is a template repository to quickly get you started developing your own Gitea API-consuming Go application.

A quick guide to use this template:

1. create a new repo based on this template to get started:
    [!['use this template' button](.gitea/use_template.png)][create-from-template]
2. install a recent golang version
3. check out and run the [example](main.go):
    ```
    git clone https://gitea.com/mnnna/mnnnb.git
    cd mnnnb
    GITEA_TOKEN=<your token here> go run .
    ```
    You can get an API token from your Gitea account settings.
    If you feel adventurous, modify this client instead to obtain a token through username/password authentication!
4. [check out the documentation][sdk-docs] of Gitea's [go-sdk][go-sdk]
5. write your own awesome client!
6. add your application to [awesome-gitea][awesome]

[create-from-template]: https://gitea.com/repo/create?template_id=19605
[go-sdk]:   https://gitea.com/gitea/go-sdk
[sdk-docs]: https://pkg.go.dev/code.gitea.io/sdk/gitea
[awesome]:  https://gitea.com/gitea/awesome-gitea

Once you created your own repo from the template, feel free to delete this disclaimer.

## template license
MIT, © The Gitea Authors

---

# mnnnb

go client interacting with [Gitea](https://gitea.io)'s API

> This repo was bootstrapped with the [template-go-client](/noerw/template-go-client) template.

## development
```
make
```

## license
- vendor/**: see respective licenses
