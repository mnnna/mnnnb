package main

import (
	"fmt"
	"os"

	"code.gitea.io/sdk/gitea"
)

func main() {
	token := os.Getenv("GITEA_TOKEN")
	host := os.Getenv("GITEA_HOST")

	if token == "" {
		fmt.Println("env GITEA_TOKEN required")
		return
	}
	if host == "" {
		host = "https://gitea.com"
	}

	client, err := gitea.NewClient(host, gitea.SetToken(token))
	if err != nil {
		fmt.Printf("couldn't create gitea client: %s\n", err)
		return
	}

	user, _, err := client.GetMyUserInfo()
	if err != nil {
		fmt.Printf("couldn't get user info: %s\n", err)
		return
	}

	fmt.Printf("Hello %s!\n", user.UserName)

	ver, _, err := client.ServerVersion()
	if err != nil {
		fmt.Printf("couldn't get server info: %s\n", err)
		return
	}

	fmt.Printf("Gitea server at %s runs v%s\n", host, ver)
}
